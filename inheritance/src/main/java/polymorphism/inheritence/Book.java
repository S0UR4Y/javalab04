package polymorphism.inheritence;

public class Book {
    protected String name;
    private String author;

    public String getName() {
        return this.name;
    }

    public String authorAuthor() {
        return this.author;
    }
    public Book(String name, String author){
        this.name=name;
        this.author=author;
    }
    public String toString(){
        return "Book: "+this.name+" author: "+this.author;
    }
  public class ElectronicBook extends Book{
        private int numberBytes;
        public ElectronicBook(String name, String author, int fileSize) {
            super(name, author); 
            this.numberBytes = fileSize;
        }
        public int getNumberBytes(){
            return this.numberBytes;
        } 
        public String toString(){
            return super.toString()+" Bytes"+ this.numberBytes;
        }
    }
}
