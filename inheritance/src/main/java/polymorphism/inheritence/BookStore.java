package polymorphism.inheritence;

import polymorphism.inheritence.Book.ElectronicBook;

public class BookStore {
    public static void main(String[] args) {
        Book[] books = new Book[5];
        books[0] = new Book("real1", "areal1");
        books[2] = new Book("real2", "areal2");
        books[1] = books[0].new ElectronicBook("d1", "ad1", 1);
        books[3] = books[0].new ElectronicBook("d2", "ad2", 2);
        books[4] = books[0].new ElectronicBook("d3", "ad3", 3);
        
        ElectronicBook eBookTest= (ElectronicBook)books[0];
        System.out.println(eBookTest);
    }
}
